/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lapxo2;
import java.util.Scanner;

/**
 *
 * @author USER
 */
public class LapXo2 {
    static char[][]table = {{'-','-','-'},
                            {'-','-','-'},
                            {'-','-','-'}};
    static char Currentplayer ='X';
    static int row,col;
    static void PrintWelcome(){
        System.out.println("Welcome to TIC TAC TOE Game");
    }
      static void Table(){
       for(int i=0;i<3;i++){
           for(int j=0;j<3;j++){
               System.out.print(table[i][j]+" ");
           }
           System.out.println();
       }
    }
    static void Turn(){
        System.out.println(Currentplayer+" Turn");
    }
     static void inputRowCol(){
        Scanner kb = new Scanner(System.in);
        while(true){
        System.out.print("Please input Row Col :");
        row = kb.nextInt();
        col = kb.nextInt();
        if(table[row-1][col-1] == '-'){
        table[row-1][col-1] = Currentplayer;
        break;
        }    
       }
    }
     static boolean isWin(){
     while(true){  
        if(checkRow()){
        return true;                     
        }if(checkCol()){
         return true;
        }if(checkDiag()){
         return true;
        }
        return false;            
    }
}
   static boolean checkRow(){
        for(int i=0; i<3; i++){
            if (table[i][0] != '-' && table[i][0] == table[i][1] && table[i][1] == table[i][2]) {
                return true; 
            }
        }return false;
        
    }
    
    static boolean checkCol(){
        for(int i=0; i<3; i++){
            if (table[0][i] != '-' && table[0][i] == table[1][i] && table[1][i] == table[2][i]) {
                return true;
            }
        }return false;
    }
    
    static boolean checkDiag(){
        return (table[0][0] != '-' && table[0][0] == table[1][1] && table[1][1] == table[2][2])
                || (table[0][2] != '-' && table[0][2] == table[1][1] && table[1][1] == table[2][0]);
    }
    static boolean isDraw(){
        for(int i =0; i<3;i++){
            for(int j = 0; j<3; j++){
                if(table[i][j] == '-'){
                    return false;
                }
            }
        }
        return true;
    }

      static void switchplayer(){
        if(Currentplayer == 'X'){
            Currentplayer ='O';
        }else{
            Currentplayer ='X';
        }
    }
     static void printWin(){
        System.out.println(Currentplayer + " is Winner!!");
    }
    
    /*static void reGame(){
    table = new char[][]{{'-','-','-'},
                            {'-','-','-'},
                            {'-','-','-'}};
    Currentplayer ='X';
    }*/
    
 
    public static void main(String[] args) {
        PrintWelcome();
        while(true){
        Table();
        Turn();
        inputRowCol();
        if(isWin()){
           Table();
           printWin();
           break;
        }
        if(isDraw()){
            Table();
            System.out.println("It's draw!!");
            break;         
        }
        switchplayer();
      
       
       }
    }
}
  


    
